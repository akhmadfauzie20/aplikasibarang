<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Models\Item;
use Faker\Generator as Faker;

$factory->define(Item::class, function (Faker $faker) {
    return [
        'code' => 'BRG-00' . random_int(1, 10),
        'name' => $faker->userName,
    ];
});
