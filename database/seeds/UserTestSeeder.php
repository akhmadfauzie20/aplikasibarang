<?php

use App\Models\User;
use Illuminate\Database\Seeder;

class UserTestSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(User::class, 1)->create([
            'username' => 'adminbarang',
            'created_by' => random_int(1, 10),
        ]);
    }
}
