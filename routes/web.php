<?php

use Illuminate\Support\Facades\Artisan;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Route::get('/clear-cache', function() {
    Artisan::call('cache:clear');
});


Route::get('/composer-dump', function()
{
    Artisan::call('dump-autoload');
    echo 'dump-autoload complete';
});

Route::middleware('guest')->get('/', function () {
    return redirect(route('login'));
});

Route::prefix('/auth')->namespace('Auth')->group(function () {

    Route::get('/login', 'LoginController@showLoginForm')->name('login');
    Route::get('/logout', 'LoginController@logout')->name('logout');
    Route::post('/login', 'LoginController@login');

    Route::get('/register', 'RegisterController@showRegisterForm')->name('register');
    Route::post('/register', 'RegisterController@register');

});

Route::get('/img', 'Dashboard\DownloadController@getImage')->name('img.any');

Route::middleware('auth')->namespace('Dashboard')->group(function () {

    Route::get('/dashboard', 'DashboardController@index')->name('dashboard.index');

    Route::prefix('/item')->group(function () {
        Route::post('/create', 'DashboardController@create')->name('item.create');
        Route::post('/update', 'DashboardController@update')->name('item.update');
        Route::delete('/delete/{id?}', 'DashboardController@delete')->name('item.delete');
    });

    Route::prefix('/cart')->group(function () {
        Route::get('/', 'ItemCartController@index')->name('cart.index');
        Route::post('/create', 'ItemCartController@create')->name('cart.create');
        Route::post('/confirm', 'ItemCartController@confirm')->name('cart.confirm');
        Route::post('/update', 'ItemCartController@update')->name('cart.update');
        Route::delete('/delete/{id?}', 'ItemCartController@delete')->name('cart.delete');
    });

    Route::prefix('/history')->group(function () {
        Route::get('/', 'ItemHistoryController@index')->name('history.index');
        Route::post('/create', 'ItemHistoryController@create')->name('history.create');
        Route::delete('/delete/{id}', 'ItemHistoryController@delete')->name('history.delete');
    });

});
