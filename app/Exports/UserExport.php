<?php

namespace App\Exports;

use Illuminate\Contracts\View\View;
use Maatwebsite\Excel\Concerns\Exportable;
use Maatwebsite\Excel\Concerns\FromView;

class UserExport implements FromView
{
    use Exportable;

    public $data;

    public function __construct($data)
    {
        $this->data = $data['result'];
    }

    /**
     * @inheritDoc
     */
    public function view(): View
    {
        return view('pages.user-export', [
            'data_list' => $this->data
        ]);
    }
}
