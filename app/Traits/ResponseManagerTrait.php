<?php
/**
 * Created by PhpStorm.
 * User: Diandraa
 * Date: 2019-03-12
 * Time: 1:27 AM
 */

namespace App\Traits;


trait ResponseManagerTrait
{

    public function simpleMessage($message, $code = 200)
    {
        return response([
            'message' => $message
        ], $code);
    }

    public function payloadMessage($message, $payload, $code = 200)
    {
        return response([
            'message' => $message,
            'payload' => $payload
        ], $code);
    }

    public function url()
    {
        $url = 'https://tailside.hrismci.com';

        return $url;
    }

    public function devUrl()
    {
         $devUrl = 'http://127.0.0.1:8000';

        return $devUrl;
    }

    public function imgUrl()
    {
        $imgUrl = 'https://hrismci.com/img?q=';

        return $imgUrl;
    }

    public function devImgUrl()
    {
        $imgUrl = 'http://127.0.0.1:8000';

        return $imgUrl;
    }
}
