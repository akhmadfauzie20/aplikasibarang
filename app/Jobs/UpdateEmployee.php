<?php

namespace App\Jobs;

use App\Entities\Karyawan;
use App\Entities\User;
use App\Utils\KaryawanUtil;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Spatie\Permission\Models\Role;

class UpdateEmployee implements ShouldQueue
{
    use Dispatchable;

    protected $request;

    protected $updatedUser;

    public function __construct(Request $request)
    {
        $this->rules($request)->validate();
        $this->request = $request;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $this->updatedUser = User::query()->findOrFail($this->request->id);

        $this->updatedUser->update($this->request->only(['name', 'username']));

        if (!empty($this->request->password)) {
            $this->updatedUser->password = bcrypt($this->request->password);
            $this->updatedUser->save();
        }

        if (!empty($this->request->role)) {
            if ($this->request->role == "HRD") {
                $newRole = Role::findByName($this->request->role);
            }

            if ($this->request->role == "IT") {
                $newRole = Role::findByName($this->request->role);
            }

            if ($this->request->role == "EDITOR") {
                $newRole = Role::findByName($this->request->role);
            }

            // Delete old role
            $this->updatedUser->roles()->detach();

            // Attach new role
            $this->updatedUser->assignRole($newRole);
        }
    }

    protected function rules(Request $request)
    {
        $roles = join(',', [KaryawanUtil::DIVISION_HRD, KaryawanUtil::DIVISION_IT, KaryawanUtil::DIVISION_EDITOR]);
        return Validator::make($request->all(), [
            'id' => 'required|exists:users,id',
            'name' => 'required',
            'username' => 'required',
            'password' => '',
            'role' => 'in:' . $roles
        ]);
    }
}
