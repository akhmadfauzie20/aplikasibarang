<?php

namespace App\Jobs;

use App\Entities\Karyawan;
use App\Entities\User;
use App\Utils\KaryawanUtil;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Spatie\Permission\Models\Role;

class CreateEmployee implements ShouldQueue
{
    use Dispatchable;

    protected $request;

    public $newUser;

    public function __construct(Request $request)
    {
        $this->rules($request)->validate();

        $this->request = $request;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $password = $this->request->input('password', 'password');

        $this->newUser = User::query()->create(array_merge(
            $this->request->only(['name', 'username']),
            ['password' => bcrypt($password)]
        ));

        $this->newUser = $this->newUser->fresh();

        // setup roles
        $role = $this->newUser->assignRole($this->request->role);

    }

    protected function rules(Request $request)
    {
        return Validator::make($request->all(), [
            'name' => 'required',
            'username' => 'required|unique:users,username',
            'password' => '',
        ]);
    }
}
