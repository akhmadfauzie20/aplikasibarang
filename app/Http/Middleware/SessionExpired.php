<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Session\Store;

class SessionExpired
{
    public function __construct(Store $session)
    {
        $this->session = $session;
    }

    public function handle($request, Closure $next)
    {
        if ($this->session->has('api_token')) {
            return $next($request);
        }

        abort(403);
    }
}
