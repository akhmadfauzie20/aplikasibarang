<?php

namespace App\Http\Controllers\Dashboard;

use App\Http\Controllers\Controller;
use App\Models\Item;
use App\Models\ItemCart;
use App\Models\ItemHistory;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class ItemCartController extends Controller
{
    public function index()
    {
        $items = Item::all();
        $carts = ItemCart::query()->where('user_id', Auth::user()->id)->get();

        return view('pages.cart', [
            'items' => $items,
            'carts' => $carts
        ]);
    }

    public function create(Request $request)
    {
        $balance = 0;
        $code = Item::query()->where('code', $request->code)->first();

        if (empty($code)) {
            alert()->error('Kode tidak ada.', 'Maaf')->persistent('OK');
            return redirect()->back();
        }

        $item = Item::query()->where('id', $code->id)->first();

        if ($request->type == 1) {
            $item->update([
                'quantity' => $request->amount + $item->quantity
            ]);

            $balance = $balance + $item->quantity;
        } elseif ($request->type == 0) {
            if ($item->quantity - intval($request->amount) == 0) {
                alert()->error('Stok tidak cukup.', 'Maaf')->persistent('OK');
                return redirect()->back();
            }

            $item->update([
                'quantity' => $item->quantity - $request->amount
            ]);

            $balance = $balance + $item->quantity;
        }

        $cart = ItemCart::query()->create([
            'item_id' => $code->id,
            'user_id' => $request->user_id,
            'proven_number' => $request->proven_number,
            'type' => $request->type,
            'amount' => $request->amount,
            'balance' => $balance,
            'date' => $request->date,
        ]);

        if ($cart->save()) {
            alert()->success('Mutasi berhasil dibuat.', 'Success')->persistent('OK');
            return redirect()->back();
        }
    }

    public function confirm()
    {
        $carts = ItemCart::query()->where('user_id', Auth::user()->id)->get();

        if (!empty($carts)) {
            foreach ($carts as $cart) {
                $history = ItemHistory::query()->create([
                    'proven_number' => $cart->proven_number,
                    'item_id' => $cart->item_id,
                    'type' => $cart->type,
                    'date' => $cart->date,
                    'amount' => $cart->amount,
                    'balance' => $cart->balance
                ]);
            }

            if ($history->save()) {
                ItemCart::query()->where('user_id', Auth::user()->id)->truncate();

                alert()->success('Mutasi berhasil disimpan.', 'Success')->persistent('OK');
                return redirect()->back();
            }
        }

        alert()->error('Barang tidak ada.', 'ERROR')->persistent('OK');
        return redirect()->back();
    }

    public function update(Request $request)
    {
        $balance = 0;
        $cart = ItemCart::query()->where('id', $request->id)
                                 ->where('user_id', $request->user_id)
                                 ->first();

        $item = Item::query()->where('id', $cart->item_id)->first();

        if (!empty($cart)) {
            if ($request->type == 1) {
                    $total = $request->amount;

                    $item->update([
                        'quantity' => $item->quantity - $cart->amount + intval($total)
                    ]);

                    $balance = $balance + $item->quantity;
            } elseif ($request->type == 0) {
                if ($item->quantity - intval($request->amount) == 0) {
                    alert()->error('Stok tidak cukup.', 'Maaf')->persistent('OK');
                    return redirect()->back();
                }

                $item->update([
                    'quantity' => $item->quantity + $cart->amount - intval($request->amount)
                ]);

                $balance = $balance + $item->quantity;
            }

            $cart->update($request->except(['balance' => $balance]));
        }

        alert()->error('Barang tidak ada.', 'ERROR')->persistent('OK');
        return redirect()->back();
    }

    public function delete($item_id)
    {
        $cart = ItemCart::query()->where('item_id', $item_id)->first();
        $item = Item::query()->where('id', $item_id)->first();

        if (!empty($cart)) {
            $cart->delete();

            $item->update([
                'quantity' => $item->quantity - $cart->amount
            ]);

            alert()->success('item berhasil dihapus', 'success')->persistent('OK');
            return redirect()->back();
        }

        alert()->error('Maaf, barang tidak ada', 'error')->persistent('OK');
        return redirect()->back();
    }
}
