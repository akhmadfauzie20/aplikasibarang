<?php

namespace App\Http\Controllers\Dashboard;

use App\Http\Controllers\Controller;
use App\Models\Item;
use App\Models\ItemHistory;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Http\Request;

class ItemHistoryController extends Controller
{
    public function index(Request $request)
    {
        $items = Item::all();
        $histories = ItemHistory::query();

        $date_start = $request->date_start;
        $date_end = $request->date_end;
        $q = $request->q;

        if (isset($date_start) && isset($date_end) && isset($q)) {
            $histories->whereDate('date', '>=', $date_start)
                      ->whereDate('date', '<=', $date_end)
                      ->whereHas('item', function (Builder $item) use ($q) {
                            $item->where('name', 'LIKE', "%$q%");
                      });

        } elseif (isset($date_start) && isset($date_end) && empty($q)) {
            $histories->whereDate('date', '>=', $date_start)
                      ->whereDate('date', '<=', $date_end);
        } elseif (empty($date_start) && empty($date_end) && isset($q)) {
            $histories->whereHas('item', function (Builder $item) use ($q) {
                         $item->where('name', 'LIKE', "%$q%");
                      });
        }

        $histories_result = $histories->latest()->get();

        return view('pages.history', [
            'histories' => $histories_result,
            'items' => $items
        ]);
    }

    public function create(Request $request)
    {
        $item = Item::query()->where('id', $request->item_id)->first();

        if ($request->type == 0) {
            if ($item->stock - $request->amount == 0) {
                alert()->error('Maaf, stok dibawah minimal.', 'error')->persistent('coba lagi');
                return redirect()->back();
            }

            $item->stock = $item->stock - $request->amount;
            $item->save();
        } else {
            $item->stock = $item->stock + $request->amount;
            $item->save();
        }

        $history = ItemHistory::query()->create([
            'item_id' => $request->item_id,
            'type' => $request->type,
            'amount' => $request->amount,
        ]);

        if ($history->save()) {
            alert()->success('Transaksi barang berhasil dibuat', 'success')->persistent('selesai');
            return redirect()->back();
        }

        alert()->error('Maaf, ada yang salah saat melakukan transaksi', 'error')->persistent('coba lagi');
        return redirect()->back();
    }
}
