<?php

namespace App\Http\Controllers\Dashboard;

use App\Models\Item;
use App\Models\ItemHistory;
use App\Traits\ResponseManagerTrait;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use UxWeb\SweetAlert\SweetAlert as Alert;

class DashboardController extends Controller
{
    use ResponseManagerTrait;

    public function index()
    {
        $items = Item::query()->get();

        return view('pages.dashboard', [
            'items' => $items
        ]);
    }

    public function create(Request $request)
    {
        $codes = Item::query()->where('code', $request->code)->first();

        if (!empty($codes)) {
            alert()->error('Kode telah digunakan.', 'error')->persistent('OK');

            return redirect()->back();
        }

        $item = Item::query()->create([
            'code' => $request->code,
            'name' => $request->name,
        ]);

        if ($item->save()) {
            $item = ItemHistory::query()->create([
                'item_id' => $item->id,
                'type' => 1,
                'amount' => $item->stock,
                'created_by' => $request->user_id
            ]);

            alert()->success('Barang berhasil dibuat', 'success')->persistent('selesai');
            return redirect()->back();
        }

        alert()->error('Maaf, ada yang salah saat membuat barang', 'error')->persistent('Coba lagi');
        return redirect()->back();
    }

    public function update(Request $request)
    {
        $item = Item::query()->where('code', $request->code)->first();

        if (!empty($item)) {
            alert()->error('Kode telah digunakan.', 'ERROR')->persistent('OK');
            return redirect()->back();
        }

        $item->update($request->all());

        alert()->success('Barang berhasil diubah', 'success')->persistent('OK');
        return redirect()->back();
    }

    public function delete($id)
    {
        $item = Item::query()->findOrFail($id);

        if (!empty($item)) {
            $item->delete();

            $item->update([
                'deleted_by' => Auth::user()->id
            ]);

            alert()->success('Barang berhasil dihapus', 'success')->persistent('selesai');
            return redirect()->back();
        }

        alert()->error('Maaf, barang tidak ada', 'error')->persistent('Coba lagi');
        return redirect()->back();
    }
}
