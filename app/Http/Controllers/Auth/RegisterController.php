<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Traits\AuthorizableRegister;
use App\Traits\ResponseManagerTrait;
use Exception;
use GuzzleHttp\Client;
use GuzzleHttp\Exception\ClientException;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class RegisterController extends Controller
{
    use AuthorizableRegister, ResponseManagerTrait;

    public function showRegisterForm()
    {
        try {
            // Company List
            $getCompany = new Client();
            $url = $this->devUrl() . '/api/auth/company';

            $headers = [
                'Accept' => 'application/json',
            ];

            $doGet = $getCompany->get($url, [
                'headers' => $headers,
            ]);

            $result = $doGet->getBody()->getContents();
            $company = json_decode($result, true);

            return view('auth.register', [
                'company_list' => $company
            ]);
        } catch (ClientException $e) {
            session()->flush();
            return redirect()->back()->with(['error' => 'Maaf, telah terjadi kesalahan, silahkan hubungi cs']);
        } catch (\ErrorException $e) {
            session()->flush();
            return redirect()->back()->with(['error' => 'Maaf, telah terjadi kesalahan, silahkan hubungi cs']);
        } catch (Exception $e) {
            session()->flush();
            return redirect()->back()->with(['error' => 'Maaf, telah terjadi kesalahan, silahkan hubungi cs']);
        }
    }

    public function register(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'password' => 'required|same:password2',
        ]);

        if ($validator->fails()) {
            return redirect()->back()->with(['error' => $validator->errors()]);
        }

//        if(!empty($request->image_android)) {
//            $file = $request->image_android;
//            $store = $file->store('profile');
//            $storageName = $store;
//        }
//
//        if (!empty($request->image)) {
//            $image = $request->input('image'); // image base64 encoded
//            preg_match("/data:image\/(.*?);/",$image,$image_extension); // extract the image extension
//            $image = preg_replace('/data:image\/(.*?);base64,/','',$image); // remove the type part
//            $image = str_replace(' ', '+', $image);
//            $imageName = "profile" . '.' . $image_extension[1]; //generating unique file name;
//            Storage::put( 'profile/' . ucwords(strtolower($request->nama_pegawai)) . ' - ' . $imageName, base64_decode($image));
//            $storageName = 'profile/' . ucwords(strtolower($request->nama_pegawai)) . ' - ' . $imageName;
//        }
        if (!isset($request->image)) return redirect()->back()->with(['error' => 'Dimohon untuk memasukkan foto dengan valid.']);

        $date = strtotime($request->tgl_lahir);
        $newDate = date('Y/m/d', $date);

        $url = $this->devUrl() . '/api/auth/register';

        $headers = [
            'Accept' => 'application/json',
        ];

        $register = new Client();

        $doRegister = $register->post($url, [
            'headers' => $headers,
            'form_params' => [
                'name' => ucwords(strtolower($request->nama_pegawai)),
                'nickname' => $request->nickname,
                'address' => $request->alamat_pegawai,
                'ktp_address' => $request->alamat_ktp,
                'birth_date' => $newDate,
                'gender' => $request->jenis_kelamin,
                'phone' => $request->no_telp,
                'email' => strtolower($request->email),
                'password' => $request->password,
                'img' => $request->input('image'),
                'company_id' => $request->company,
//                  'territory' => $request->wilayah
            ]
        ]);

        $registerRes = $doRegister->getBody()->getContents();
        $regisJs = json_decode($registerRes, true);

        if ($regisJs['status'] == true) {
            return redirect()->back()->with(['success' => 'User Berhasil Dibuat.']);
        }

        return redirect()->back()->with(['error' => 'Maaf, Email telah dipakai.']);
    }
}
