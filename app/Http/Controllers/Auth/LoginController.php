<?php

namespace App\Http\Controllers\Auth;

use App\Models\User;
use App\Traits\ResponseManagerTrait;
use Exception;
use GuzzleHttp\Client;
use GuzzleHttp\Exception\ClientException;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Traits\AuthorizableLogin;
use Illuminate\Support\Facades\Session;
use UxWeb\SweetAlert\SweetAlert as Alert;

class LoginController extends Controller
{
    use AuthorizableLogin, ResponseManagerTrait;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest')->except('logout');
    }

    public function showLoginForm()
    {
        return view('auth.login');
    }

    public function login(Request $request)
    {
        $this->validateLogin($request);

        if (!$this->existUsername($request)) {
            Alert::error('Maaf, username tidak tersedia.', 'error');
            return redirect()->back();
        }

        if ($this->attemptLogin($request)) {
            return $this->sendLoginResponse($request);
        }

        $this->attemptLogout($request);

        return $this->sendFailedLoginResponse($request, trans('auth.login.failed.password'));
    }

    public function logout(Request $request)
    {
        return $this->attemptLogout($request);
    }
}
