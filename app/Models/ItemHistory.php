<?php

namespace App\Models;

use DateTimeInterface;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class ItemHistory extends Model
{
    use SoftDeletes;

    protected $table = 'item_histories';

    protected $fillable = [
        'item_id',
        'proven_number',
        'type',
        'amount',
        'balance',
        'date',
        'deleted_by',
    ];

    protected $dates = [
        'date',
        'created_at',
        'updated_at',
        'deleted_at'
    ];

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function item()
    {
        return $this->belongsTo(Item::class);
    }

    public function serializeDate(DateTimeInterface $date)
    {
        return $this->attributes['created_at']->format('Y-m-d');
    }
}
