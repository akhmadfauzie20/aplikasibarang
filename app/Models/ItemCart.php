<?php

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

class ItemCart extends Model
{
    protected $table = 'item_carts';

    protected $fillable = [
        'user_id',
        'item_id',
        'proven_number',
        'type',
        'amount',
        'balance',
        'date',
    ];

    protected $dates = [
        'date',
        'created_at',
        'updated_at',
    ];

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function item()
    {
        return $this->belongsTo(Item::class);
    }

    public function serializeDate(\DateTimeInterface $date)
    {
        return $date->format('Y-m-d');
    }
}
