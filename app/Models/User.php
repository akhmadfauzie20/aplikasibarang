<?php

namespace App\Models;

use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    use SoftDeletes;

    protected $table = 'users';

    protected $fillable = [
        'name',
        'username',
        'password',
        'created_by',
        'updated_by',
        'deleted_by',
    ];

    protected $dates = [
        'created_at',
        'updated_at',
    ];

    public function item()
    {
        return $this->hasMany(Item::class);
    }

    public function item_log()
    {
        return $this->hasMany(ItemHistory::class);
    }
}
