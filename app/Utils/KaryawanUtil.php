<?php
/**
 * Created by PhpStorm.
 * User: Diandraa
 * Date: 2019-03-06
 * Time: 2:55 PM
 */

namespace App\Utils;


class KaryawanUtil
{

    public const DIVISION_HRD = "HRD";
    public const DIVISION_EDITOR = "EDITOR";
    public const DIVISION_IT = "IT";

    public const NAME_ALIASES = [
        'hrd' => self::DIVISION_HRD,
        'editor' => self::DIVISION_EDITOR,
        'it' => self::DIVISION_IT,
    ];

    public static function alias($alias)
    {
        try {
            return self::NAME_ALIASES[$alias];
        } catch (\OutOfBoundsException $e) {
            return null;
        }
    }

}
