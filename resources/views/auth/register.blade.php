<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, shrink-to-fit=no" name="viewport">
    <title>HRIS &mdash; Daftar</title>

    <!-- General CSS Files -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.7.2/css/all.css" integrity="sha384-fnmOCqbTlWIlj8LyTjo7mOUStjsKC4pOpQbqyi7RrhN7udi9RwhKkMHpvLbHG9Sr" crossorigin="anonymous">

    <!-- Template CSS -->
    <link rel="stylesheet" href="{{ url('css/style.css') }}">
<!--<link rel="stylesheet" href="{{ url('css/bootstrap.min.css') }}">-->
    <link rel="stylesheet" href="{{ url('css/components.css') }}">

    <?php
    $userAgent=$_SERVER['HTTP_USER_AGENT'];

    if(preg_match('/(android|bb\d+|meego).+mobile|avantgo|bada\/|blackberry|blazer|compal|elaine|fennec|hiptop|iemobile|ip(hone|od)|iris|kindle|lge |maemo|midp|mmp|netfront|opera m(ob|in)i|palm( os)?|phone|p(ixi|re)\/|plucker|pocket|psp|series(4|6)0|symbian|treo|up\.(browser|link)|vodafone|wap|windows (ce|phone)|xda|xiino/i',$userAgent)||preg_match('/1207|6310|6590|3gso|4thp|50[1-6]i|770s|802s|a wa|abac|ac(er|oo|s\-)|ai(ko|rn)|al(av|ca|co)|amoi|an(ex|ny|yw)|aptu|ar(ch|go)|as(te|us)|attw|au(di|\-m|r |s )|avan|be(ck|ll|nq)|bi(lb|rd)|bl(ac|az)|br(e|v)w|bumb|bw\-(n|u)|c55\/|capi|ccwa|cdm\-|cell|chtm|cldc|cmd\-|co(mp|nd)|craw|da(it|ll|ng)|dbte|dc\-s|devi|dica|dmob|do(c|p)o|ds(12|\-d)|el(49|ai)|em(l2|ul)|er(ic|k0)|esl8|ez([4-7]0|os|wa|ze)|fetc|fly(\-|_)|g1 u|g560|gene|gf\-5|g\-mo|go(\.w|od)|gr(ad|un)|haie|hcit|hd\-(m|p|t)|hei\-|hi(pt|ta)|hp( i|ip)|hs\-c|ht(c(\-| |_|a|g|p|s|t)|tp)|hu(aw|tc)|i\-(20|go|ma)|i230|iac( |\-|\/)|ibro|idea|ig01|ikom|im1k|inno|ipaq|iris|ja(t|v)a|jbro|jemu|jigs|kddi|keji|kgt( |\/)|klon|kpt |kwc\-|kyo(c|k)|le(no|xi)|lg( g|\/(k|l|u)|50|54|\-[a-w])|libw|lynx|m1\-w|m3ga|m50\/|ma(te|ui|xo)|mc(01|21|ca)|m\-cr|me(rc|ri)|mi(o8|oa|ts)|mmef|mo(01|02|bi|de|do|t(\-| |o|v)|zz)|mt(50|p1|v )|mwbp|mywa|n10[0-2]|n20[2-3]|n30(0|2)|n50(0|2|5)|n7(0(0|1)|10)|ne((c|m)\-|on|tf|wf|wg|wt)|nok(6|i)|nzph|o2im|op(ti|wv)|oran|owg1|p800|pan(a|d|t)|pdxg|pg(13|\-([1-8]|c))|phil|pire|pl(ay|uc)|pn\-2|po(ck|rt|se)|prox|psio|pt\-g|qa\-a|qc(07|12|21|32|60|\-[2-7]|i\-)|qtek|r380|r600|raks|rim9|ro(ve|zo)|s55\/|sa(ge|ma|mm|ms|ny|va)|sc(01|h\-|oo|p\-)|sdk\/|se(c(\-|0|1)|47|mc|nd|ri)|sgh\-|shar|sie(\-|m)|sk\-0|sl(45|id)|sm(al|ar|b3|it|t5)|so(ft|ny)|sp(01|h\-|v\-|v )|sy(01|mb)|t2(18|50)|t6(00|10|18)|ta(gt|lk)|tcl\-|tdg\-|tel(i|m)|tim\-|t\-mo|to(pl|sh)|ts(70|m\-|m3|m5)|tx\-9|up(\.b|g1|si)|utst|v400|v750|veri|vi(rg|te)|vk(40|5[0-3]|\-v)|vm40|voda|vulc|vx(52|53|60|61|70|80|81|83|85|98)|w3c(\-| )|webc|whit|wi(g |nc|nw)|wmlb|wonu|x700|yas\-|your|zeto|zte\-/i',substr($userAgent,0,4))) {
        $userAgent = true;
    } else {
        $userAgent = false;
    }
    ?>

    <style>
        #my_camera{
            width: 100% !important;
            height: 300px !important;
            border: 1px solid transparent;
        }
    </style>
</head>

<body>
<div id="app">

    @if ($message = Session::get('success'))
        <div class="alert alert-success alert-dismissible fade show" role="alert">
            <strong>{{ $message }}</strong>
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
        </div>
    @endif

    @if ($message = Session::get('error'))
        <div class="alert alert-danger alert-dismissible fade show" role="alert">
            <strong>{{ $message }}</strong>
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
        </div>
    @endif

    <form action="{{ route('register') }}" method="POST" enctype="multipart/form-data">
        @csrf
        <section class="section">
            <div class="container mt-5">
                <div class="row">
                    <div class="col-12 col-sm-10 offset-sm-1 col-md-8 offset-md-2 col-lg-8 offset-lg-2 col-xl-8 offset-xl-2">
                        <div class="login-brand">
                            <a href="{{ url('/') }}">
                                <img src="{{ url('/assets/img/stisla-fill.svg') }}" alt="logo" width="100"
                                     class="shadow-light rounded-circle">
                            </a>
                        </div>

                        @if ($userAgent)
                            <div class="alert alert-danger alert-dismissible fade show" role="alert">
                                <strong>Tolong gunakan PC / Laptop anda</strong>
                                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                            </div>
                        @endif

                        <div class="card card-primary" id="container_android">
                            <div class="card-header">
                                <h4>Register</h4>
                            </div>
                            <div class="card-body">
                                <div class="row">
                                    <div class="form-group col-6">
                                        <label for="name">Nama Lengkap</label>
                                        <input id="name" type="text" class="form-control input_android" name="nama_pegawai" placeholder="Masukkan nama anda" required>
                                        <div class="invalid-feedback">
                                        </div>
                                    </div>

                                    <div class="form-group col-6">
                                        <label for="nickname">Nama Panggilan</label>
                                        <input id="nickname" type="text" class="form-control input_android" name="nickname" placeholder="Masukkan nama panggilan" required>
                                        <div class="invalid-feedback">
                                        </div>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label for="address">Alamat saat ini</label>
                                    <input id="address" type="text" class="form-control input_android" name="alamat_pegawai" placeholder="Masukkan alamat tempat tinggal anda saat ini" required>
                                    <div class="invalid-feedback">
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label for="address">Alamat KTP</label>
                                    <input id="address" type="text" class="form-control input_android" name="alamat_ktp" placeholder="Masukkan alamat tempat tinggal anda di ktp" required>
                                    <div class="invalid-feedback">
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="form-group col-6">
                                        <label for="bd" class="d-block">Tanggal Lahir</label>
                                        <input id="bd" type="date" class="form-control input_android" name="tgl_lahir" onkeydown="false" placeholder="Masukkan tanggal / bulan / tahun lahir anda" required>
                                        <div class="invalid-feedback">
                                        </div>
                                    </div>

                                    <div class="form-group col-6">
                                        <label for="phone">No. HP</label>
                                        <input id="phone" type="text" class="form-control input_android" name="no_telp" onkeypress="return hanyaAngka(event)" maxlength="15" placeholder="Masukkan No. HP aktif" required>
                                        <div class="invalid-feedback">
                                        </div>
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="form-group col-6">
                                        <label for="gender" class="d-block">Jenis Kelamin</label>
                                        <select class="custom-select custom-select-sm form-control input_android" id="gender" name="jenis_kelamin" required>
                                            <option value="" selected>Pilih Jenis Kelamin</option>
                                            <option value="Pria">Pria</option>
                                            <option value="Wanita">Wanita</option>
                                        </select>
                                    </div>

                                    <div class="form-group col-6">
                                        <label for="company">Nama Perusahaan</label>
                                        <select class="custom-select custom-select-sm form-control input_android" id="company" name="company" required>
                                            <option value="" selected>- Pilih Perusahaan -</option>
                                            @foreach($company_list['result'] as $company)
                                                <option value="{{ $company['id'] }}">{{ $company['name'] }}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label for="email">Email</label>
                                    <input id="email" type="email" class="form-control input_android" name="email" placeholder="Masukkan E-mail anda." required>
                                    <div class="invalid-feedback">
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label for="password">Password</label>
                                    <input id="password" type="password" class="form-control input_android" name="password" required>
                                    <div class="invalid-feedback">
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label for="password2">Konfirmasi Password</label>
                                    <input id="password2" type="password" class="form-control input_android" name="password2" required>
                                    <div class="invalid-feedback">
                                    </div>
                                </div>

                                <div class="form-group">
                                    <button type="button" id="btn-camera" class="btn btn-primary btn-lg btn-block input_android" data-toggle="modal" data-target="#cameraModal">
                                        Ambil Foto
                                    </button>
                                </div>
                            </div>
                        </div>

                        <div class="simple-footer">
                            Copyright &copy; {{ env('APP_NAME') }} <script>document.write(new Date().getFullYear())</script><br/>
                            {{--                        <small>Version {{ env('APP_VERSION') }}</small>--}}
                        </div>
                    </div>
                </div>
            </div>
        </section>

        {{-- Card Form --}}
        <div class="modal fade" id="cameraModal" tabindex="-1" role="dialog" aria-labelledby="cameraModal" aria-hidden="true" style="z-index: 9999;">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalLabel">Take A Photo</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <div id="accordion">
                            <div class="card text-black bg-primary mb-3">
                                <div class="card-header " id="catatanPanduan" class="btn" type="button" data-toggle="collapse" data-target="#catatanPanduanCollapse" aria-expanded="true" aria-controls="catatanPanduanCollapse">
                                    <h6 class="mb-0">
                                        <!--<button class="btn" type="button" data-toggle="collapse" data-target="#collapseOne" aria-expanded="true" aria-controls="collapseOne">-->
                                        Panduan & Catatan <b>Take A Photo</b>
                                        <!--</button>-->
                                        </h6>
                                </div>

                                <div id="catatanPanduanCollapse" class="collapse" aria-labelledby="catatanPanduan" data-parent="#accordion">
                                    <div class="card-body">
                                        <p>Panduan : </p>
                                        <ol>
                                        <li>Pastikan data telah terisi dengan lengkap.</li>
                                        <li>Aktifkan permission / izin akses kamera oleh browser.</li>
                                        <li>Klik tombol "Turn On Camera" untuk menyalakan kamera.</li>
                                        <li>Klik tombol "Take Photo" untuk mengambil foto.</li>
                                        <li>Klik tombol "Use Photo" untuk menggunakan foto tersebut.</li>
                                        </ol>
                                        <hr style="height: 1px; color: #FFFFFF; background-color: #FFFFFF;"/>
                                        <p>Catatan : </p>
                                        <ul>
                                            <li class="text-red">Jika terdapat error lakukan <b>Refresh</b> dan ulangi panduan secara berurutan.</li>
                                            <li class="text-red">Pastikan <b>Javascript</b> pada browser aktif (true).</li>
                                            <li class="text-red">Kontak pengembang aplikasi.</li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="form-group" style="text-align: center;">
                            <div class="pages camera-wrapper">
                                <div id="my_camera" class="camera"></div>
                                <div id="results" class="results"></div> <br>
                                <div class="btn-group button-cam">
                                    <input type="button" class="btn btn-sm btn-primary item-cam" value="Turn On Camera" onClick="configure()" style="margin-right: 5px; border-radius: .25rem;">
                                    <input type="button" class="btn btn-sm btn-primary item-cam" value="Take Photo" onClick="take_snapshot()" style="margin-left: 5px; margin-right: 5px; border-radius: .25rem;">
                                    <input type="hidden" id="file-input" name="image" value="">
                                    <input type=submit class="btn btn-sm btn-primary mr-2" value="Use Photo" style="margin-left: 5px; border-radius: .25rem;">
                                </div>
                            </div>

                            <div class="camera-android">
                                <input type="file" accept="image/*" name="image_android" capture="camera" id="capture">
                                <div class="btn-group take-photo">
                                    <button type="submit" class="btn btn-sm btn-primary">
                                        Save
                                    </button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        {{-- Card Form End --}}
    </form>
</div>

<!-- General JS Scripts -->
<!--<script src="https://code.jquery.com/jquery-3.3.1.min.js" integrity="sha256-FgpCb/KJQlLNfOu91ta32o/NMZxltwRo8QtmkMRdAu8=" crossorigin="anonymous"></script>-->
<!--<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>-->
<!--<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>-->
<!--<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery.nicescroll/3.7.6/jquery.nicescroll.min.js"></script>-->
<!--<script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.24.0/moment.min.js"></script>-->

<!-- Template JS File -->
<script src="{{ url('js/jquery.min.js') }}"></script>
<script src="{{ url('js/popper.min.js') }}"></script>
<script src="{{ url('js/scripts.js') }}"></script>
<script src="{{ url('js/bootstrap.min.js') }}"></script>
<script src="{{ url('js/custom.js') }}"></script>
<script src="{{ url('js/jquery.nicescroll.min.js') }}"></script>
<script src="{{ url('js/webcam.min.js') }}"></script>
<script src="{{ asset('js/moment.min.js') }}"></script>

<script>
    if( /Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent) ) {
        // $('#container_android').css({
        //     filter: "blur(2px)";
        //     -webkit-filter: "blur(2px)";
        // })
        $('#container_android').attr("style", "filter: blur(2px); -webkit-filter: blur(2px); -ms-filter: blur(2px); -moz-filter: blur(2px); -o-filter: blur(2px);")
        $('.input_android').attr("disabled", "true");
    } else {
        $('.camera-android').hide();
    }

    $('#cameraModal').on('shown.bs.modal', function () {
        $('#btn-camera').trigger('focus')
    })

    function forceLower(evt) {
        // Get an array of all the words (in all lower case)
        var words = evt.target.value.toLowerCase().split(/\s+/g);

        // Loop through the array and replace the first letter with a cap
        var newWords = words.map(function(element){
            // As long as we're not dealing with an empty array element, return the first letter
            // of the word, converted to upper case and add the rest of the letters from this word.
            // Return the final word to a new array
            return element !== "" ?  element[0].toUpperCase() + element.substr(1, element.length) : "";
        });

        // Replace the original value with the updated array of capitalized words.
        evt.target.value = newWords.join(" ");
    }

    var camera = document.getElementById('my_camera');
    var results = document.getElementById('results');

    function configure() {
        Webcam.set({
            width: 460,
            height: 330,
            image_format: 'jpeg',
            jpeg_quality: 90,
        },'constraint', {
            video:true,
            facingMode: "environtment"
        });
        Webcam.attach(camera);

        $('.camera').show();
        $('.results').hide();
    }
    // A button for taking snaps

    // preload shutter audio clip
    var shutter = new Audio();
    shutter.autoplay = false;
    // shutter.src = navigator.userAgent.match(/Firefox/) ? 'shutter.ogg' : 'shutter.mp3';

    function take_snapshot() {
        $('.camera').hide();
        $('.results').show();
        // play sound effect
        // shutter.play();

        // take snapshot and get image data
        Webcam.snap( function(data_uri) {
            // display results in page
            document.getElementById('results').innerHTML =
                '<img id="imageprev" src="'+data_uri+'"/>';
        } );

        Webcam.reset();

        saveSnap();
    }

    function saveSnap() {
        // Get base64 value from <img id='imageprev'> source
        var base64image = document.getElementById("imageprev").src;
        // console.log(base64image);

        $('#file-input').val(base64image);
    }

    function hanyaAngka(evt) {
        var charCode = (evt.which) ? evt.which : event.keyCode
        if (charCode > 31 && (charCode < 48 || charCode > 57))

            return false;
        return true;
    }
</script>

</body>
</html>
