@extends('scaffold')
<?php
    $title = 'Inventory';
?>
@section('page-title', $title)
@section('content-title', $title)

@section('content-breadcrumbs')
@endsection

@section('content')
    <style>
        .my-dropdown {
            overflow-y: scroll;
            max-height: 200px;
        }

        ul.pagination {
            float: right;
        }

        .page-link {
            background-color: red;
        }

        div#table-list_filter{
            display: none;
        }

        div#table-list_info{
            margin-left: 15px;
        }

        div#table-list_length{
            margin-left: 15px;
        }

        select[name="table-list_length"] {
            width: 100px;
        }

        div#table-pending_filter{
            display: none;
        }

        div#table-pending_info{
            margin-left: 15px;
            margin-top: 10px;
        }

        div#table-pending_length{
            margin-left: 15px;
        }

        select[name="table-pending_length"] {
            width: 100px;
        }

        table#table-pending {
            margin-bottom: 10px;
        }
    </style>

{{-- User List Start --}}
    <div class="card">
    <div class="card-header">
        <h4 style="color: #34395e;">{{ $title }}</h4>
        <div class="card-header-action">
{{--            <a href="{{ route('history.index') }}" class="btn btn-sm btn-secondary ml-1 mr-1" style="border-radius: .2rem;">--}}
{{--                <i class="fas fa-history"></i>&nbsp;&nbsp;&nbsp;&nbsp;Cek Mutasi--}}
{{--            </a>--}}

            <a href="{{ route('history.index') }}" class="btn btn-sm btn-secondary ml-1 mr-1" style="border-radius: .2rem;">
                <i class="fas fa-file-import"></i>&nbsp;&nbsp;&nbsp;&nbsp;Report Mutasi
            </a>

            <a href="{{ route('cart.index') }}" class="btn btn-sm btn-secondary ml-1 mr-1" style="border-radius: .2rem;">
                            <i class="fas fa-history"></i>&nbsp;&nbsp;&nbsp;&nbsp;Buat Mutasi
            </a>

            <a href="#" class="btn btn-danger" style="margin-top: -2px;" data-toggle="modal" data-target="#createModal">
                <i class="fas fa-plus"></i>&nbsp;&nbsp;&nbsp;&nbsp;Tambah Data
            </a>
        </div>
    </div>
    <!--<div class="table-responsive">-->
    <table class="table table-bordered" id="table-list">
        <thead>
        <tr>
            <th scope="col" class="text-center">No.</th>
            <th scope="col">Kode Barang</th>
            <th scope="col">Nama Barang</th>
            <th scope="col" class="text-center">Aksi</th>
        </tr>
        </thead>
        <tbody>
        @forelse ($items as $item)
            <tr>
                <td class="text-center">{{ $loop->iteration }}</td>
                <td>{{ $item->code }}</td>
                <td>{{ $item->name }}</td>
                <td class="text-center">
                    <div class="btn-group">
                        <a href="#"
                           class="btn btn-sm btn-secondary btn-for-edit mr-1"
                           id="btn-edit"
                           data-id="{{ $item->id }}"
                           data-code="{{ $item->code }}"
                           data-name="{{ $item->name }}"
                           style="border-radius: .2rem;"
                        >
                            <i class="fas fa-pen"></i>
                        </a>
                        <form action="{{ route('item.delete', $item->id) }}" method="post">
                            {{ csrf_field() }} {{ method_field('DELETE') }}
                            <button class="btn btn-sm btn-secondary ml-1" type="submit" onclick="return confirm('Yakin ingin menghapus data?')" style="border: .2rem;"><i class="fas fa-trash"></i></button>
                        </form>
                    </div>
                </td>
            </tr>
        @empty
            <tr>
                <td class="text-center" colspan="8">Tidak ada barang.</td>
            </tr>
        @endforelse
        </tbody>
    </table>
    <!--</div>-->
</div>
{{-- User List End --}}

<script type="text/javascript" src="https://cdn.datatables.net/v/bs4/jq-3.3.1/dt-1.10.21/datatables.min.js"></script>

<script>
    $(document).ready(function () {
        $('#table-list').DataTable({});
    });

    // Edit
    $('.btn-for-edit').each(function (index) {
        $(this).on('click', function (e) {
            console.log('clicked');
            let id = $(this).data('id');
            let code = $(this).data('code');
            let name = $(this).data('name');

            $('#id').val(id);
            $('#code-show').val(code);
            $('#name-show').val(name);

            $('#editModal').modal("show");
        });
    });
</script>
@endsection

@section('script')
    <div class="modal fade" id="createModal" role="dialog" aria-labelledby="createModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Tambah Data Baru</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <form action="{{ route('item.create') }}" method="post">
                        @csrf
                        <div class="form-group">
                            <label for="code">Kode Barang</label>
                            <input type="text" id="code" name="code" class="form-control" placeholder="Masukkan kode barang" maxlength="7" required>

                            <input type="hidden" id="user_id" name="user_id" class="form-control" value="{{ \Illuminate\Support\Facades\Auth::user()->id }}" required>
                        </div>

                        <div class="form-group">
                            <label for="name">Nama Barang</label>
                            <input type="text" id="name" name="name" class="form-control" placeholder="Masukkan nama barang" required>
                        </div>

                        <div class="row">
                            <div class="col-md-12">
                                <div class="button-group">
                                    <button type="submit" class="btn btn-danger float-right mr-2">Simpan</button>
                                    <button type="button" class="btn btn-secondary float-right mr-2" data-dismiss="modal">Batal</button>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>

    <div class="modal fade" id="editModal" role="dialog" aria-labelledby="editModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Edit Data</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <form action="{{ route('item.update') }}" method="post">
                        @csrf
                        <div class="form-group">
                            <label for="code">Kode Barang</label>
                            <input type="text" id="code-show" name="code" class="form-control" placeholder="Masukkan kode barang" maxlength="7" required>

                            <input type="hidden" id="id" name="item_id" class="form-control" required>
                            <input type="hidden" id="user_id" name="user_id" class="form-control" value="{{ \Illuminate\Support\Facades\Auth::user()->id }}" required>
                        </div>

                        <div class="form-group">
                            <label for="name">Nama Barang</label>
                            <input type="text" id="name-show" name="name" class="form-control" placeholder="Masukkan nama barang" required>
                        </div>

                        <div class="col-md-12">
                            <div class="button-group">
                                <button type="submit" class="btn btn-danger float-right mr-2">Simpan</button>
                                <button type="button" class="btn btn-secondary float-right mr-2" data-dismiss="modal">Batal</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>

<script>
    function onlyNumber(evt) {
        var charCode = (evt.which) ? evt.which : event.keyCode
        if (charCode > 31 && (charCode < 48 || charCode > 57))

            return false;
        return true;
    }
</script>
@endsection
