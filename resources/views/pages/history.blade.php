@extends('scaffold')
<?php
    $title = 'Report Mutasi';
?>
@section('page-title', $title)
@section('content-title-display', 'none')
@section('content-back-display', 'inline-block')
@section('content-back-link', route('dashboard.index'))

@section('content-breadcrumbs')
    @include('components.breadcrumb-item', ['text' => 'Inventory', 'active' => true, 'link' => route('dashboard.index')])
    @include('components.breadcrumb-item', ['text' => 'History', 'active' => false])
@endsection

@section('content')
    <style>
        .my-dropdown {
            overflow-y: scroll;
            max-height: 200px;
        }

        ul.pagination {
            float: right;
        }

        .page-link {
            background-color: red;
        }

        div#table-list_filter{
            display: none;
        }

        div#table-list_info{
            margin-left: 15px;
        }

        div#table-list_length{
            margin-left: 15px;
        }

        select[name="table-list_length"] {
            width: 100px;
        }

        .page-link {
            border-color: transparent;
            background-color: #f9fafe;
            color: #34395e;
        }

        div#table-pending_filter{
            display: none;
        }

        div#table-pending_info{
            margin-left: 15px;
            margin-top: 10px;
        }

        div#table-pending_length{
            margin-left: 15px;
        }

        select[name="table-pending_length"] {
            width: 100px;
        }

        table#table-pending {
            margin-bottom: 10px;
        }
    </style>

{{-- User List Start --}}
    <div class="card">
    <div class="card-header">
        <h4 style="color: #34395e;">{{ $title }}</h4>
        <div class="card-header-action">
            <form class="d-inline-flex"
                  action="{{ route('history.index', array_merge(app('request')->all())) }}"
                  method="GET">
                    <input type="text"
                           class="form-control datepicker"
                           name="date_start"
                           value="{{ app('request')->input('date_start', '') }}"
                           placeholder="Pick start date"
                           style="max-width: 150px; border-radius: 30px 0 0 30px !important;">

                    <input type="text"
                           class="form-control datepicker"
                           name="date_end"
                           value="{{ app('request')->input('date_end', '') }}"
                           placeholder="Pick end date"
                           style="max-width: 150px; border-radius: 0 30px 30px 0 !important;">

                    <input type="text"
                           class="form-control"
                           name="q"
                           value="{{ app('request')->input('q', '') }}"
                           placeholder="Search..."
                           style="max-width: 150px; margin-left: 10px; border-radius: 30px 0 0 30px !important;">

                <div class="input-group-btn">
                    <button type="submit" class="btn btn-search btn-danger" style="border-radius: 0 30px 30px 0 !important; margin-right:20px; margin-top: 0 !important;"><i class="fas fa-search"></i></button>
                </div>
            </form>
        </div>
    </div>
    <!--<div class="table-responsive">-->
    <table class="table table-bordered" id="table-list">
        <thead>
        <tr>
            <th scope="col" class="text-center">Tanggal</th>
            <th scope="col">No. Bukti</th>
            <th scope="col">Barang</th>
            <th scope="col">IN</th>
            <th scope="col">OUT</th>
            <th scope="col">SALDO</th>
        </tr>
        </thead>
        <tbody>
        @forelse ($histories as $history)
            <tr>
                <td class="text-center">{{ $history->date->format('Y-m-d') }}</td>
                <td>{{ $history->proven_number }}</td>
                <td>{{ $history->item->name }}</td>
                <td><p>{{ $history->type == 1 ? $history->amount : "" }}  </p></td>
                <td><p>{{ $history->type == 0 ? $history->amount : "" }}  </p></td>
                <td>{{ $history->balance }}</td>
            </tr>
        @empty
            <tr>
                <td class="text-center" colspan="8">Belum ada transaksi.</td>
            </tr>
        @endforelse
        </tbody>
    </table>
    <!--</div>-->
</div>
{{-- User List End --}}

<script type="text/javascript" src="https://cdn.datatables.net/v/bs4/jq-3.3.1/dt-1.10.21/datatables.min.js"></script>

<script>
    $(document).ready(function () {
        $('#table-list').DataTable({});
    });

    $(document).ready(function() {
        $('.datepicker').datepicker({
            dateFormat: 'yy-mm-dd',
        });
    });
</script>
@endsection

@section('script')
    <div class="modal fade" id="createModal" role="dialog" aria-labelledby="createModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Tambah Data Baru</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <form action="{{ route('history.create') }}" method="post">
                        @csrf
                        <div class="form-group">
                            <label for="code">Pilih Barang</label>
                            <select class="form-control custom-select custom-select-sm" name="item_id" style="display: inline-block;
                                    border-radius: 0 0 0 0
                                    !important; padding:1px 15px; height: 32px; margin-top: -1px;" id="item_id">
                                <option value="" selected>- Pilih Barang -</option>
                                @foreach(@$items as $item)
                                    <option value="{{ @$item->id }}">{{ @$item->name }}</option>
                                @endforeach
                            </select>
                        </div>

                        <div class="form-group">
                            <label for="code">Jenis Transaksi</label>
                            <input type="hidden" id="user_id" name="user_id" class="form-control" value="{{ \Illuminate\Support\Facades\Auth::user()->id }}" required>
                        </div>

                        <div class="radio-group" style="margin-top: -25px; margin-bottom: 25px;">
                            <div class="form-check form-check-inline">
                                <input class="form-check-input" type="radio" name="type" id="inlineRadio1" value="1">
                                <label class="form-check-label" for="inlineRadio1">Menambah</label>
                            </div>
                            <div class="form-check form-check-inline">
                                <input class="form-check-input" type="radio" name="type" id="inlineRadio2" value="0">
                                <label class="form-check-label" for="inlineRadio2">Mengurangi</label>
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="amount">Jumlah</label>
                            <input type="text" id="amount" name="amount" class="form-control" placeholder="Masukkan jumlah" onkeypress="onlyNumber(event)" required>
                        </div>

                        <div class="row">
                            <div class="col-md-12">
                                <div class="button-group">
                                    <button type="submit" class="btn btn-danger float-right mr-2">Simpan</button>
                                    <button type="button" class="btn btn-secondary float-right mr-2" data-dismiss="modal">Batal</button>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
<script>
    function onlyNumber(evt) {
        var charCode = (evt.which) ? evt.which : event.keyCode
        if (charCode > 31 && (charCode < 48 || charCode > 57))

            return false;
        return true;
    }
</script>
@endsection
