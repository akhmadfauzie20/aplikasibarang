@extends('scaffold')
<?php
    $title = 'Mutasi Barang';
?>
@section('page-title', $title)
@section('content-title-display', 'none')
@section('content-back-display', 'inline-block')
@section('content-back-link', route('dashboard.index'))

@section('content-breadcrumbs')
    @include('components.breadcrumb-item', ['text' => 'Inventory', 'active' => true, 'link' => route('dashboard.index')])
    @include('components.breadcrumb-item', ['text' => 'Mutate', 'active' => false])
@endsection

@section('content')
    <style>
        .my-dropdown {
            overflow-y: scroll;
            max-height: 200px;
        }

        ul.pagination {
            float: right;
        }

        .page-link {
            background-color: red;
        }

        div#table-list_filter{
            display: none;
        }

        div#table-list_info{
            margin-left: 15px;
        }

        div#table-list_length{
            margin-left: 15px;
        }

        select[name="table-list_length"] {
            width: 100px;
        }

        .page-link {
            border-color: transparent;
            background-color: #f9fafe;
            color: #34395e;
        }

        div#table-pending_filter{
            display: none;
        }

        div#table-pending_info{
            margin-left: 15px;
            margin-top: 10px;
        }

        div#table-list_info{
            display: none;
        }

        select[name="table-pending_length"] {
            width: 100px;
        }

        table#table-pending {
            margin-bottom: 10px;
        }
    </style>

{{-- User List Start --}}
    <div class="card">
    <div class="card-header">
        <h4 style="color: #34395e;">{{ $title }}</h4>
        <div class="card-header-action">
            <a href="#" class="btn btn-danger" style="margin-top: -2px;" data-toggle="modal" data-target="#createModal">
                <i class="fas fa-plus"></i>&nbsp;&nbsp;&nbsp;&nbsp;Tambah Data
            </a>
        </div>
    </div>
    <!--<div class="table-responsive">-->
    <table class="table table-bordered" id="table-list">
        <thead>
        <tr>
            <th scope="col" class="text-center">No.</th>
            <th scope="col">No. Bukti</th>
            <th scope="col">Tanggal</th>
            <th scope="col">Kode Barang</th>
            <th scope="col">Jenis Transaksi</th>
            <th scope="col">Amount</th>
            <th scope="col">Aksi</th>
        </tr>
        </thead>
        <tbody>
        @forelse ($carts as $cart)
            <tr>
                <td class="text-center">{{ $loop->iteration }}</td>
                <td>{{ $cart->proven_number }}</td>
                <td>{{ $cart->date->format('Y-m-d') }}</td>
                <td>{{ $cart->item->code }}</td>
                <td><p style="color: {{ $cart->type == 0 ? "red" : "blue" }}">{{ $cart->type == 0 ? "Mengurangi" : "Menambah" }}  </p></td>
                <td>{{ $cart->amount }}</td>
                <td class="text-center">
                    <div class="btn-group">
                        <a href="#"
                           class="btn btn-sm btn-secondary btn-for-edit mr-1"
                           id="btn-edit"
                           data-id="{{ $cart->id }}"
                           data-proven="{{ $cart->proven_number }}"
                           data-date="{{ $cart->date->format('m/d/Y') }}"
                           data-code="{{ $cart->item->code }}"
                           data-type="{{ $cart->type }}"
                           data-amount="{{ $cart->amount }}"
                           style="border-radius: .2rem;"
                        >
                            <i class="fas fa-pen"></i>
                        </a>
                        <form action="{{ route('cart.delete', $cart->id) }}" method="post">
                            {{ csrf_field() }} {{ method_field('DELETE') }}
                            <button class="btn btn-sm btn-secondary ml-1" type="submit" onclick="return confirm('Yakin ingin menghapus data?')" style="border: .2rem;"><i class="fas fa-trash"></i></button>
                        </form>
                    </div>
                </td>
            </tr>
        @empty
            <tr>
                <td class="text-center" colspan="8">Belum ada transaksi.</td>
            </tr>
        @endforelse
        </tbody>
    </table>
    <!--</div>-->
    @if($carts != '[]')
    <div class="col-md-12 col-md-5">
        <form action="{{ route('cart.confirm') }}" method="post">
            {{ csrf_field() }} {{ method_field('POST') }}
            <button class="btn btn-sm btn-danger btn-secondary ml-1" type="submit" onclick="return confirm('Yakin ingin menyimpan data?')" style="border-radius: .2rem; margin-top: -90px;"> Simpan Mutasi <i class="fas fa-history"></i></button>
        </form>
    </div>
    @endif
</div>
{{-- User List End --}}

<script type="text/javascript" src="https://cdn.datatables.net/v/bs4/jq-3.3.1/dt-1.10.21/datatables.min.js"></script>

<script>
    $(document).ready(function () {
        $('#table-list').DataTable({});
    });

    $('.btn-for-edit').each(function () {
        $(this).on('click', function (e) {
            let id = $(this).data('id');
            let proven = $(this).data('proven');
            let date = $(this).data('date');
            let code = $(this).data('code');
            let type = $(this).data('type');
            let amount = $(this).data('amount');

            $('#id').val(id);
            $('#proven_show').val(proven);
            $('#date_show').val(date);
            $('#code_show').val(code);

            if (type == 1) {
                $('#inlineRadio1Show').prop("checked", true);
            } else {
                $('#inlineRadio2Show').prop("checked", true);
            }

            $('#amount_show').val(amount);

            $('#editModal').modal("show");
        });
    })
</script>
@endsection

@section('script')
    <div class="modal fade" id="createModal" role="dialog" aria-labelledby="createModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Tambah Data Baru</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <form action="{{ route('cart.create') }}" method="post">
                        @csrf
                        <div class="form-group">
                            <label for="code">No Bukti</label>
                            <input type="text" id="proven_num" name="proven_number" class="form-control" value="" onkeypress="return onlyNumber(event)" required>
                        </div>

                        <div class="form-group">
                            <label for="code">Tanggal</label>
                            <input type="text" id="date" name="date" class="form-control datepicker" required>
                        </div>

                        <div class="form-group">
                            <label for="code">Kode Barang</label>
                            <input type="text" id="code" name="code" class="form-control" required>
                        </div>

                        <div class="form-group">
                            <label for="code">Jenis Transaksi</label>
                            <input type="hidden" id="user_id" name="user_id" class="form-control" value="{{ \Illuminate\Support\Facades\Auth::user()->id }}" required>
                        </div>

                        <div class="radio-group" style="margin-top: -25px; margin-bottom: 25px;">
                            <div class="form-check form-check-inline">
                                <input class="form-check-input" type="radio" name="type" id="inlineRadio1" value="1">
                                <label class="form-check-label" for="inlineRadio1">Menambah</label>
                            </div>
                            <div class="form-check form-check-inline">
                                <input class="form-check-input" type="radio" name="type" id="inlineRadio2" value="0">
                                <label class="form-check-label" for="inlineRadio2">Mengurangi</label>
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="amount">Jumlah</label>
                            <input type="text" id="amount" name="amount" class="form-control" placeholder="Masukkan jumlah" onkeypress="onlyNumber(event)" required>
                        </div>

                        <div class="row">
                            <div class="col-md-12">
                                <div class="button-group">
                                    <button type="submit" class="btn btn-danger float-right mr-2">Simpan</button>
                                    <button type="button" class="btn btn-secondary float-right mr-2" data-dismiss="modal">Batal</button>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>

    <div class="modal fade" id="editModal" role="dialog" aria-labelledby="editModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Tambah Data Baru</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <form action="{{ route('cart.update') }}" method="post">
                        @csrf
                        <div class="form-group">
                            <label for="proven_show">No Bukti</label>
                            <input type="text" id="proven_show" name="proven_number" class="form-control" value="" onkeypress="return onlyNumber(event)" required>
                        </div>

                        <div class="form-group">
                            <label for="date_show">Tanggal</label>
                            <input type="text" id="date_show" name="date" class="form-control datepicker" required>
                        </div>

                        <div class="form-group">
                            <label for="code_show">Kode Barang</label>
                            <input type="text" id="code_show" name="code" class="form-control" required>
                        </div>

                        <div class="form-group">
                            <label for="code">Jenis Transaksi</label>
                            <input type="hidden" id="user_id" name="user_id" class="form-control" value="{{ \Illuminate\Support\Facades\Auth::user()->id }}" required>
                            <input type="hidden" id="id" name="id" class="form-control" required>
                        </div>

                        <div class="radio-group" style="margin-top: -25px; margin-bottom: 25px;">
                            <div class="form-check form-check-inline">
                                <input class="form-check-input" type="radio" name="type" id="inlineRadio1Show" value="1">
                                <label class="form-check-label" for="inlineRadio1">Menambah</label>
                            </div>
                            <div class="form-check form-check-inline">
                                <input class="form-check-input" type="radio" name="type" id="inlineRadio2Show" value="0">
                                <label class="form-check-label" for="inlineRadio2">Mengurangi</label>
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="amount">Jumlah</label>
                            <input type="text" id="amount_show" name="amount" class="form-control" placeholder="Masukkan jumlah" onkeypress="onlyNumber(event)" required>
                        </div>

                        <div class="row">
                            <div class="col-md-12">
                                <div class="button-group">
                                    <button type="submit" class="btn btn-danger float-right mr-2">Ubah</button>
                                    <button type="button" class="btn btn-secondary float-right mr-2" data-dismiss="modal">Batal</button>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>

<script>
    $(document).ready(function() {
        $('.datepicker').datepicker({
            dateFormat: 'yy-mm-dd',
        });
    });

    function onlyNumber(evt) {
        var charCode = (evt.which) ? evt.which : event.keyCode
        if (charCode > 31 && (charCode < 48 || charCode > 57))

            return false;
        return true;
    }
</script>
@endsection
