<?php
function shouldHasActive($route)
{
    $current = \Illuminate\Support\Facades\Route::currentRouteName();
    return $current == $route ? 'active' : '';
}

function dropdownShouldHasActive($arrayRoute)
{
    $current = \Illuminate\Support\Facades\Route::currentRouteName();
    //$xxx = \Illuminate\Support\Facades\Route::();
    //dd(in_array($current, $arrayRoute) ? 'active' : '');
    return in_array($current, $arrayRoute) ? 'active' : '';
}

$role = Session::get('role');
?>
<div class="main-sidebar sidebar-style-2">
    <aside id="sidebar-wrapper">
        <div class="sidebar-brand">
            <a href="{{ route('dashboard.index') }}">HRIS</a>
        </div>
        <div class="sidebar-brand sidebar-brand-sm">
            <a href="{{ route('dashboard.index') }}">HR</a>
        </div>
        <ul class="sidebar-menu">
            <li class="nav-item {{ shouldHasActive('dashboard.index') }}">
                <a href="{{ route('dashboard.index') }}" class="nav-link"><i class="fas fa-fire"></i>
                    <span>Dashboard</span></a>
            </li>

            <li class="menu-header">Features</li>

            @if($role == "ADMIN")
                <li class="dropdown {{ dropdownShouldHasActive(['employee.index', 'employee.export']) }}">
                    <a href="#" class="nav-link has-dropdown" data-toggle="dropdown"><i class="fas fa-users"></i> <span>Karyawan</span></a>
                    <ul class="dropdown-menu">
                        <li class="{{ shouldHasActive('employee.index') }}">
                            <a class="nav-link" href="{{ route('employee.index') }}"><i class="fas fa-user"></i>Daftar Karyawan</a>
                        </li>

                        <li class="{{ shouldHasActive('employee.export') }}">
                            <a class="nav-link" href="{{ route('employee.export') }}"><i class="fas fa-file-export"></i> Export Karyawan</a>
                        </li>
                    </ul>
                </li>

                <li class="dropdown {{ dropdownShouldHasActive(['presence.index', 'presence.import']) }}">
                    <a href="#" class="nav-link has-dropdown" data-toggle="dropdown"><i class="fas fa-business-time"></i> <span>Presensi</span></a>
                    <ul class="dropdown-menu">
                        <li class="{{ shouldHasActive('presence.index') }}">
                            <a href="{{ route('presence.index') }}"><i class="fas fa-id-card"></i>Daftar Presensi</a>
                        </li>

                        <li class="{{ shouldHasActive('presence.import') }}">
                            <a class="nav-link" href="{{ route('presence.import') }}"><i class="fas fa-file-import"></i>Import Presensi</a>
                        </li>
                    </ul>
                </li>

                <li class="dropdown">
                    <a href="#" class="nav-link has-dropdown {{ dropdownShouldHasActive(['payroll.index', 'karyawan.export']) }}" data-toggle="dropdown"><i class="fas fa-file-invoice-dollar"></i> <span>Payroll</span></a>
                    <ul class="dropdown-menu">
                        <li>
                            <a class="nav-link {{ shouldHasActive('payroll.index') }}" href="{{ route('payroll.index') }}"><i class="fas fa-money-check"></i>Payroll</a>
                        </li>
                    </ul>
                </li>

                <li class="dropdown">
                    <a href="#" class="nav-link has-dropdown {{ dropdownShouldHasActive(['holiday.index', 'flow.index']) }}" data-toggle="dropdown"><i class="fas fa-calendar"></i> <span>Cuti</span></a>
                    <ul class="dropdown-menu">

                        <li><a class="nav-link {{ shouldHasActive('holiday.index') }}" href="{{ route('holiday.index') }}"><i class="fas fa-calendar-minus"></i>Daftar Cuti</a></li>

                        <li><a class="nav-link {{ shouldHasActive('flow.index') }}" href="{{ route('flow.index') }}"><i class="fas fa-chart-bar"></i>Arus Cuti</a></li>
                    </ul>
                </li>

                <li class="dropdown">
                    <a href="#" class="nav-link has-dropdown {{ dropdownShouldHasActive(['overtime.index', 'karyawan.export']) }}" data-toggle="dropdown"><i class="fas fa-portrait"></i> <span>Lembur</span></a>
                    <ul class="dropdown-menu">
                        <li class="{{ shouldHasActive('overtime.index') }}">
                            <a class="nav-link" href="{{ route('overtime.index') }}"><i class="fas fa-user-clock"></i>Daftar Lembur</a>
                        </li>
                    </ul>
                </li>

                <li class="dropdown {{ dropdownShouldHasActive(['permit.index']) }}">
                    <a href="#" class="nav-link has-dropdown" data-toggle="dropdown"><i class="fas fa-users-cog"></i> <span>Izin</span></a>
                    <ul class="dropdown-menu">
                        <li class="{{ shouldHasActive('permit.index') }}">
                            <a class="nav-link" href="{{ route('permit.index') }}"><i class="fas fa-bed"></i>Daftar Izin</a>
                        </li>
                    </ul>
                </li>

                <li class="dropdown {{ dropdownShouldHasActive(['division.index']) }}">
                    <a href="#" class="nav-link has-dropdown" data-toggle="dropdown"><i class="fas fa-cogs"></i> <span>Pengaturan</span></a>
                    <ul class="dropdown-menu">
                        <li class="{{ shouldHasActive('division.index') }}">
                            <a class="nav-link" href="{{ route('division.index') }}"><i class="fas fa-users-cog"></i>Divisi</a>
                        </li>

                        <li class="{{ shouldHasActive('company.index') }}">
                            <a class="nav-link" href="{{ route('company.index') }}"><i class="fas fa-city"></i>Perusahaan</a>
                        </li>

                        {{--                    <li><a class="nav-link" href="{{route('division.index')}}"><i class="fas fa-file"></i> Position</a></li>--}}

                        {{--                    <li><a class="nav-link" href="{{route('division.index')}}"><i class="fas fa-file"></i> Company</a></li>--}}

                    </ul>
                </li>
            @endif

            @if($role == "HRD")
                <li class="dropdown {{ dropdownShouldHasActive(['employee.index', 'employee.export']) }}">
                    <a href="#" class="nav-link has-dropdown" data-toggle="dropdown"><i class="fas fa-users"></i> <span>Karyawan</span></a>
                    <ul class="dropdown-menu">
                        <li class="{{ shouldHasActive('employee.index') }}">
                            <a class="nav-link" href="{{ route('employee.index') }}"><i class="fas fa-user"></i>Daftar Karyawan</a>
                        </li>

                        <li class="{{ shouldHasActive('employee.export') }}">
                            <a class="nav-link" href="{{ route('employee.export') }}"><i class="fas fa-file-export"></i> Export Karyawan</a>
                        </li>
                    </ul>
                </li>

                <li class="dropdown {{ dropdownShouldHasActive(['presence.index', 'presence.import']) }}">
                    <a href="#" class="nav-link has-dropdown" data-toggle="dropdown"><i class="fas fa-business-time"></i> <span>Presensi</span></a>
                    <ul class="dropdown-menu">
                        <li class="{{ shouldHasActive('presence.index') }}">
                            <a href="{{ route('presence.index') }}"><i class="fas fa-id-card"></i>Daftar Presensi</a>
                        </li>
                    </ul>
                </li>

                <li class="dropdown">
                    <a href="#" class="nav-link has-dropdown {{ dropdownShouldHasActive(['payroll.index', 'karyawan.export']) }}" data-toggle="dropdown"><i class="fas fa-file-invoice-dollar"></i> <span>Payroll</span></a>
                    <ul class="dropdown-menu">
                        <li>
                            <a class="nav-link {{ shouldHasActive('payroll.index') }}" href="{{ route('payroll.index') }}"><i class="fas fa-money-check"></i>Payroll</a>
                        </li>
                    </ul>
                </li>

                <li class="dropdown">
                    <a href="#" class="nav-link has-dropdown {{ dropdownShouldHasActive(['holiday.index', 'flow.index']) }}" data-toggle="dropdown"><i class="fas fa-calendar"></i> <span>Cuti</span></a>
                    <ul class="dropdown-menu">

                        <li><a class="nav-link {{ shouldHasActive('holiday.index') }}" href="{{ route('holiday.index') }}"><i class="fas fa-calendar-minus"></i>Daftar Cuti</a></li>

                        <li><a class="nav-link {{ shouldHasActive('flow.index') }}" href="{{ route('flow.index') }}"><i class="fas fa-chart-bar"></i>Arus Cuti</a></li>
                    </ul>
                </li>

                <li class="dropdown {{ dropdownShouldHasActive(['overtime.index']) }}">
                    <a href="#" class="nav-link has-dropdown" data-toggle="dropdown"><i class="fas fa-portrait"></i> <span>Lembur</span></a>
                    <ul class="dropdown-menu">
                        <li class="{{ shouldHasActive('overtime.index') }}">
                            <a class="nav-link" href="{{ route('overtime.index') }}"><i class="fas fa-user-clock"></i> Daftar Lembur</a>
                        </li>
                    </ul>
                </li>

                <li class="dropdown {{ dropdownShouldHasActive(['permit.index']) }}">
                    <a href="#" class="nav-link has-dropdown" data-toggle="dropdown"><i class="fas fa-users-cog"></i> <span>Izin</span></a>
                    <ul class="dropdown-menu">
                        <li class="{{ shouldHasActive('permit.index') }}">
                            <a class="nav-link" href="{{ route('permit.index') }}"><i class="fas fa-bed"></i>Daftar Izin</a>
                        </li>
                    </ul>
                </li>
            @endif

            @if($role == "EMPLOYEE")
                <li class="nav-item {{ shouldHasActive('employee.index') }}">
                    <a href="{{ route('employee.index') }}" class="nav-link active"><i class="fas fa-user"></i>
                        <span>Karyawan</span>
                    </a>
                </li>

                <li class="nav-item {{ shouldHasActive('holiday.index') }}">
                    <a href="{{ route('holiday.index') }}" class="nav-link active"><i class="fas fa-calendar"></i>
                        <span>Cuti</span>
                    </a>
                </li>

                <li class="nav-item {{ shouldHasActive('overtime.index') }}">
                    <a href="{{ route('overtime.index') }}" class="nav-link active"><i class="fas fa-user-clock"></i>
                        <span>Lembur</span>
                    </a>
                </li>

                <li class="nav-item {{ shouldHasActive('permit.index') }}">
                    <a href="{{ route('permit.index') }}" class="nav-link active"><i class="fas fa-bed"></i>
                        <span>Izin</span>
                    </a>
                </li>
            @endif

{{--                        <li class="nav-item {{ shouldHasActive('dashboard.holiday.index') }}">--}}
{{--                            <a href="{{ route('dashboard.holiday.index') }}" class="nav-link active"><i class="fas fa-calendar"></i>--}}
{{--                                <span>Jadwal Cuti</span>--}}
{{--                            </a>--}}
{{--                        </li>--}}
            {{--            <li class="nav-item">--}}
            {{--                <a href="#" class="nav-link active"><i class="fas fa-wallet"></i>--}}
            {{--                    <span>Payroll</span>--}}
            {{--                </a>--}}
            {{--            </li>--}}
        </ul>
    </aside>
</div>
