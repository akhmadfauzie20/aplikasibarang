<footer class="main-footer">
  <div class="footer-left">
    Copyright &copy; <script>document.write(new Date().getFullYear())</script>
    <div class="bullet"></div>
    aplikasiBarang
  </div>
  <div class="footer-right">
    {{ env('APP_VERSION') }}
  </div>
</footer>
