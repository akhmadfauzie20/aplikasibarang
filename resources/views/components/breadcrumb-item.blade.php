<div class="breadcrumb-item">
  @if ($active)
    <a href="{{ $link }}" style="color: #34395e;">{{ $text }}</a>
  @else
    {{ $text }}
  @endif
</div>
